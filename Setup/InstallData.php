<?php
/**
 *    * Short description
 *    *
 *    * @category CyberHULL
 *    * @package m2ee
 *    * @copyright Copyright (C) 2019 CyberHULL (www.cyberhull.com)
 *    * @author  Natalia Sekulich <natalia.sekulich@cyberhull.com>
 */

namespace Cyberhull\TheNews\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Cyberhull\TheNews\Model\NewsFactory;
use Cyberhull\TheNews\Model\ResourceModel\News;

/**
 * Class InstallData
 * @package Cyberhull\TheNews\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var NewsFactory
     */
//    protected $newsFactory;

    /**
     * InstallData constructor.
     * @param NewsFactory $newsFactory
     */
    public function __construct(NewsFactory $newsFactory)
    {
        $this->newsFactory = $newsFactory;
    }

    /**
     * Add 10 rows into table
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable(News::TABLE_NAME);

        if ($setup->getConnection()->isTableExists($tableName) == true) {
            foreach ($this->getNewsData() as $index => $item) {
                $setup->getConnection()->insert($tableName, $item);
            }
        }
//        $i = 0;
//        while ($i < 5) {
//            $data = [
//                'name' => "News $i Header",
//                'content' => "Some  news data ".rand(0, 9999),
//                'code' =>  rand(1000, 9999),
//                'main_img' =>  '/pub/media/catalog/category/digital.JPG',
//                'sort' =>  $i*10,
//            ];
//            $news = $this->newsFactory->create();
//            $news->setData($data)->save();
////            $data = $this->newsFactory->create()->setData($dataArr);
////            $this->newsRepository->save($data);
//            $i++;
//        }
    }

    public function getNewsData()
    {
        $data = [];
        $i = 0;
        while ($i < 5) {
            $data[] = [
                'name' => "News $i Header",
                'content' => "Some  news data ".rand(0, 9999),
                'code' =>  rand(1000, 9999),
                'main_img' =>  '/pub/media/catalog/category/digital.JPG',
                'sort' =>  $i*10,
            ];
            $i++;
//            $news = $this->newsFactory->create();
//            $news->setData($data)->save();
//            $data = $this->newsFactory->create()->setData($dataArr);
//            $this->newsRepository->save($data);
        }
        return $data;

//        if ($setup->getConnection()->isTableExists($tableName) == true) {
//            // Declare data
//            $data = [
//                [
//                    'title' => 'How to create a simple module',
//                    'summary' => 'The summary',
//                    'description' => 'The description',
//                    'created_at' => date('Y-m-d H:i:s'),
//                    'status' => 1
//                ],
//                [
//                    'title' => 'Create a module with custom database table',
//                    'summary' => 'The summary',
//                    'description' => 'The description',
//                    'created_at' => date('Y-m-d H:i:s'),
//                    'status' => 1
//                ]
//            ];
//
//            // Insert data to table
//            foreach ($data as $item) {
//                $setup->getConnection()->insert($tableName, $item);
//            }
//        }
    }
}
