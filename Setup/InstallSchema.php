<?php

namespace Cyberhull\TheNews\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
use Psr\Log\LoggerInterface;

/**
 * Class InstallSchema
 * @package Cyberhull\TheNews\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * InstallSchema constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }


    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('news_table')) {
            try {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('news_table')
                )
                    ->addColumn(
                        'news_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'Data ID'
                    )
                    ->addColumn(
                        'name',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable => false', 'LENGTH' =>255],
                        'Name'
                    )
                    ->addColumn(
                        'content',
                        Table::TYPE_TEXT,
                        65536,
                        ['nullable => true'],
                        'Content'
                    )
                    ->addColumn(
                        'code',
                        Table::TYPE_INTEGER,
                        null,
                        ['nullable => false'],
                        'Code'
                    )
                    ->addColumn(
                        'main_img',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable => true', 'LENGTH' =>255],
                        'Main Image'
                    )
                    ->addColumn(
                        'sort',
                        Table::TYPE_INTEGER,
                        null,
                        ['nullable => false', 'default' => 100],
                        'Sort'
                    )
                    ->addColumn(
                        'date_create',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'Created At'
                    )->addColumn(
                        'date_update',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                        'Updated At'
                    )
                    ->setComment('The News Table');
                $installer->getConnection()->createTable($table);
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
        $installer->endSetup();
    }
}
