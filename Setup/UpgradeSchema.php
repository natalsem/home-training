<?php

namespace Cyberhull\TheNews\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Cyberhull\TheNews\Model\ResourceModel\News;
use Psr\Log\LoggerInterface;


/**
 * Class UpgradeSchema
 * @package Cyberhull\TheNews\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * News to Store
     */
    const TABLE_NAME = 'news_store';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UpgradeSchema constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
     //   $setup->startSetup();
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.1.0', '<')) {

            try {
                if (!$setup->tableExists(self::TABLE_NAME)) {
                    $table = $installer->getConnection()->newTable(
                        $installer->getTable(self::TABLE_NAME)
                    )
                    ->addColumn(
                        'news_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                        ],
                        'News ID'
                    )
                    ->addColumn(
                        'store_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                        ],
                        'Store ID'
                    )
                    ->setComment('News in Stores');
                    $setup->getConnection()->createTable($table);
                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }


            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    self::TABLE_NAME,
                    'news_id',
                    News::TABLE_NAME,
                    'news_id'
                ),
                $setup->getTable(self::TABLE_NAME),
                'news_id',
                $setup->getTable(News::TABLE_NAME),
                'news_id',
                Table::ACTION_CASCADE
            );
        }
        $setup->endSetup();
    }
}
