<?php

namespace Cyberhull\TheNews\Model\News;

use Cyberhull\TheNews\Model\ResourceModel\News\CollectionFactory;
use Magento\Cms\Model\ResourceModel\Page\Collection;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\UrlInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Psr\Log\LoggerInterface;

/**
 * Class DataProvider
 * @package Cyberhull\TheNews\Model\News
 */
class DataProvider extends AbstractDataProvider
{

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;
    
    /**
     * Base path
     *
     * @var string
     */
    protected $basePath;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $pageCollectionFactory,
        UrlInterface $urlBuilder,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
    //    $basePath,
        $meta = [],
        $data = []
    ) {
     //   $this->basePath = $basePath;
        $this->logger = $logger;
        $this->urlBuilder = $urlBuilder;
        $this->collection = $pageCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->meta = $this->prepareMeta($this->meta);
    }

    public function getData()
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $itemId = 0;
        foreach ($items as $page) {
            $itemId = $page->getId();
            $this->loadedData[$itemId] = $page->getData();
            $itemData = $page->getData();
        }

        $data = $this->dataPersistor->get('cyberhull_thenews');
        if (!empty($data)) {
            $page = $this->collection->getNewEmptyItem();
            $page->setData($data);
            $this->loadedData[$page->getId()] = $page->getData();
            $this->dataPersistor->clear('cyberhull_thenews');
        }
        return $this->loadedData;
    }

    public function prepareMeta($meta)
    {
        return $meta;
    }
}
