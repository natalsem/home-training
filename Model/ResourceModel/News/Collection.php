<?php

namespace Cyberhull\TheNews\Model\ResourceModel\News;

use Cyberhull\TheNews\Model\News;
use Cyberhull\TheNews\Model\ResourceModel\News as ResourceModelNews;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Collection
 *
 * @package Cyberhull\TheNews\Model\ResourceModel\News
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $idFieldName = 'news_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(News::class, ResourceModelNews::class);
        $this->_map['fields'][ResourceModelNews::ID_FIELD_NAME] = 'main_table.' . ResourceModelNews::ID_FIELD_NAME;
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    /**
     * Collection constructor.
     *
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param AdapterInterface|null $connection
     * @param AbstractDb|null $resource
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        $this->_init('Cyberhull\TheNews\Model\News', 'Cyberhull\TheNews\Model\ResourceModel\News');
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * @return AbstractCollection
     */
    protected function _afterLoad()
    {
//        $this->getSelect()
//            ->from(['main_table' => ResourceModelNews::TABLE_NAME])
//            ->join(
//                ResourceModelNews::STORE_TABLE_NAME,
//                'main_table.news_id = '.ResourceModelNews::STORE_TABLE_NAME.'.news_id'
//            );
//        return $this;
        $linkField = ResourceModelNews::ID_FIELD_NAME;
        $tableName =  ResourceModelNews::STORE_TABLE_NAME;
        $linkedIds = $this->getColumnValues($linkField);
        if ($linkedIds) {
            $connection = $this->getConnection();
            $select = $connection->select()
            ->from(['main_table' => ResourceModelNews::TABLE_NAME])
            ->join(
                ResourceModelNews::STORE_TABLE_NAME,
                'main_table.' . $linkField . ' = ' . $tableName. '.' . $linkField
            );


//            $select = $connection->select()
//                ->from(['main_table' => $this->getTable($tableName)])
//                ->where('smain_table.' . $linkField . ' IN (?)', $linkedIds);
//
//            $result = $connection->fetchAll($select);            $select = $connection->select()
//                ->from(['main_table' => $this->getTable($tableName)])
//                ->where('smain_table.' . $linkField . ' IN (?)', $linkedIds);
//
            $result = $connection->fetchAll($select);
            $stores = [];
            foreach ($result as $news) {
                $stores[$news['news_id']][] = $news['store_id'];
            }

//            var_dump($stores);
//            foreach ( as $item) {
//
//            }
            if ($result) {
                foreach ($this as $item) {
                    if (isset($stores[$item->getData($linkField)])) {

                        $item->setData('store_id', $stores[$item->getData($linkField)]);
                    }
                }
            }

//            if ($result) {
//                $storesData = [];
//                foreach ($result as $storeData) {
//                    $storesData[$storeData[$linkField]][] = $storeData['store_id'];
//                    // [0[1]]=>1
//                }
//                foreach ($this as $item) {
//                    $linkedId = $item->getData($linkField);
//                    if (!isset($storesData[$linkedId])) {
//                        continue;
//                    }
//                    $item->setData('store_id', $storesData[$linkedId]);
//                }
//            }
        }
        return parent::_afterLoad();
    }
}
