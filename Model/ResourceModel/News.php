<?php

namespace Cyberhull\TheNews\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
/**
 * News module resource model class
 *
 * @category CyberHULL
 * @package  CyberHULL_CyberhullNews
 */
class News extends AbstractDb
{

    const TABLE_NAME = 'news_table';

    const STORE_TABLE_NAME = 'news_store';

    const ID_FIELD_NAME = 'news_id';

    /**
     * @var DateTime
     */
    protected $date;
    protected $filesystem;
    protected $file;

    /**
     * News constructor.
     *
     * @param Context $context
     * @param DateTime $date
     * @param Filesystem $filesystem
     * @param File $file
     */
    public function __construct(
        Context $context,
        DateTime $date,
        Filesystem $filesystem,
        File $file
    ) {
        $this->date = $date;
        $this->filesystem = $filesystem;
        $this->file = $file;
        parent::__construct($context);
    }

    /**
     * News module resource model class constructor
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'news_id');
    }

    /**
     * @param AbstractModel $object
     * @return $this|AbstractDb
     */
    protected function _beforeSave(AbstractModel $object)
    {
        $date = $this->date->gmtDate('Y.d.m H:i:s');
        if ($object->isObjectNew()) {
            $object->setCreatedAt($date);
        }

        return $this;
    }

    protected function getStoreId($newsId)
    {
        $connection= $this->getConnection();
        $table = $connection->select()
            ->from(self::TABLE_NAME)
            ->columns(['store_id'])
            ->where('news_id = ?', $newsId);
        $data = $connection->fetchAll($table);
        return $data;
    }

//    protected function _getLoadSelect($field, $value, $object)
//    {
//
//        $select = parent::_getLoadSelect($field, $value, $object);
//
//        $select->joinLeft(
//            ['t_b' => self::STORE_TABLE_NAME],
//            self::TABLE_NAME . '.news_id = t_b.news_id'
//            //, ['news_id', 'name', 'sort']
//        );
//        return $select;
//
////        $table = $this->getConnection();
////        $select = $this->getConnection()
////            ->from(self::TABLE_NAME)
////            ->columns(['store_id'])
////            ->where('news_id = ?', $newsId);
////        return $select;
//    }
}
