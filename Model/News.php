<?php

namespace Cyberhull\TheNews\Model;

use Cyberhull\TheNews\Api\Data\NewsInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class News
 * @package Cyberhull\TheNews\Model
 */
class News extends AbstractModel implements IdentityInterface, NewsInterface
{
    /**
     * Constant for CacheTag
     *
     * @var string
     */
    const CACHE_TAG = 'cyberhull_thenews';

    /**
     * Variable for CacheTag
     *
     * @var string
     */
    protected $cacheTag = 'cyberhull_thenews';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $eventPrefix = 'cyberhull_thenews';

    /**
     * Get Identities
     *
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get default value
     *
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }

    /**
     * News model class constructor
     */
    protected function _construct()
    {
        $this->_init('Cyberhull\TheNews\Model\ResourceModel\News');
    }
}
