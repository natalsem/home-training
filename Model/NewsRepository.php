<?php

namespace Cyberhull\TheNews\Model;

use Cyberhull\TheNews\Api\Data\NewsInterface;
use Cyberhull\TheNews\Api\NewsRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class NewsRepository
 * @package Cyberhull\TheNews\Model
 */
class NewsRepository implements NewsRepositoryInterface
{
    protected $newsFactory;
    protected $newsCollectionFactory;

    public function __construct(
        NewsFactory $newsFactory
    ) {
        $this->newsFactory = $newsFactory;
    }

    /**
     * @param NewsInterface $myNews
     * @return NewsInterface
     */
    public function save(NewsInterface $myNews)
    {
        $myNews->getResource()->save($myNews);
        return $myNews;
    }

    /**
     * @param NewsInterface $myNews
     * @return NewsInterface|void
     */
    public function delete(NewsInterface $myNews)
    {
        $myNews->getResource()->delete($myNews);
    }

    /**
     * @param $id
     * @return mixed|void
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $obj = $this->getById($id);
        $obj->delete();
    }

    /**
     * @param int $id
     * @return NewsInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $obj = $this->newsFactory->create();
        $obj->getResource()->load($obj, $id);
        if (!$obj->getId()) {
            throw new NoSuchEntityException(__('Unable to get #"%1"', $id));
        }
        return $obj;
    }
}

