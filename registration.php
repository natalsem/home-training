<?php
/**
 * Cyberhull_News
 *
 * @copyright Copyright (C) 2019 CyberHULL (www.cyberhull.com)
 * @author    Nikolay Shapovalov <nikolay.shapovalov@cyberhull.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cyberhull_TheNews',
    __DIR__
);
