<?php

namespace Cyberhull\TheNews\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface NewsInterface
 *
 * @package Cyberhull\TheNews\Api\Data
 */
interface NewsInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param $id
     * @return void
     */
    public function setId($id);
}
