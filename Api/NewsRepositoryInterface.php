<?php

namespace Cyberhull\TheNews\Api;

use Cyberhull\TheNews\Api\Data\NewsInterface;

/**
 * Interface NewsRepositoryInterface
 *
 * @package Cyberhull\TheNews\Api
 */
interface NewsRepositoryInterface
{
    /**
     * @param NewsInterface $myNews
     * @return NewsInterface
     */
    public function save(NewsInterface $myNews);

    /**
     * @param NewsInterface $myNews
     * @return NewsInterface
     */
    public function delete(NewsInterface $myNews);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
