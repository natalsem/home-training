<?php

namespace Cyberhull\TheNews\Block\Adminhtml\Buttons;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 *
 * @package Cyberhull\News\Block\Adminhtml\Edit\Buttons
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * GenericButton constructor.
     *
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->context = $context;
    }

    /**
     * Returns page id
     *
     * @return mixed
     */
    public function getNewsId()
    {
        return $this->context->getRequest()->getParam('news_id');
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
