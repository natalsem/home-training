<?php

namespace Cyberhull\TheNews\Block\Adminhtml\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 *
 * @package Cyberhull\News\Block\Adminhtml\Edit\Buttons
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Returns button's data
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Delete'),
            'class' => 'delete',
            'on_click' => 'deleteConfirm(\'' . __('Delete?') . '\',\'' . $this->getDeleteUrl() . '\')',
            'sort_order' => 20,
        ];

        return $data;
    }

    /**
     * Create URL for delete news
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['news_id' => $this->getNewsId()]);
    }
}

