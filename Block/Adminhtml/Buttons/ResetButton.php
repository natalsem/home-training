<?php

namespace Cyberhull\TheNews\Block\Adminhtml\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class ResetButton
 *
 * @package Cyberhull\News\Block\Adminhtml\Edit\Buttons
 */
class ResetButton implements ButtonProviderInterface
{
    /**
     * Returns button's data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload(); event.preventDefault();',
            'sort_order' => 30
        ];
    }
}
