<?php
/**
 *    * Short description
 *    *
 *    * @category CyberHULL
 *    * @package m2ee
 *    * @copyright Copyright (C) 2019 CyberHULL (www.cyberhull.com)
 *    * @author  Natalia Sekulich <natalia.sekulich@cyberhull.com>
 */

namespace Cyberhull\TheNews\Block\Adminhtml\Module\Grid;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Store\Model\System\Store;


class Form extends Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $systemStore;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('thenews_form');
        $this->setTitle(__('Edit Cyberhull "The News"'));
    }

    public function getModel()
    {
        return $this->_coreRegistry->registry('thenews_grid');
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->getModel();

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('cyberhull_thenews_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getManufacturerId()) {
            $fieldset->addField('manufacturer_id', 'hidden', ['name' => 'manufacturer_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Manufacturer Title'), 'title' => __('Manufacturer Title'), 'required' => true]
        );

        $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'style' => 'height:36em',
                'required' => true
            ]
        );

        $fieldset->addField(
            'is_enabled',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_enabled',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_enabled', '1');
        }

        $fieldset->addField(
            'is_restricted',
            'select',
            [
                'label' => __('Product Permissions'),
                'title' => __('Product Permissions'),
                'name' => 'is_restricted',
                'required' => true,
                'options' => ['1' => __('Restricted'), '0' => __('Unrestricted')]
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_restricted', '1');
        }

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
{

}