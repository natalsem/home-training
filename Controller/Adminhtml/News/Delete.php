<?php

namespace Cyberhull\TheNews\Controller\Adminhtml\News;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Cyberhull\TheNews\Model\NewsFactory;
use Cyberhull\TheNews\Model\NewsRepository;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Cyberhull\TheNews\Controller\Adminhtml\News
 */
class Delete extends Action
{
    /**
     * @var NewsFactory
     */
    protected $newsFactory;

    /**
     * @var NewsRepository
     */
    protected $newsRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param NewsFactory $newsFactory
     * @param NewsRepository $newsRepository
     */
    public function __construct(
        Context $context,
        NewsFactory $newsFactory,
        NewsRepository $newsRepository
    ) {
        $this->newsFactory = $newsFactory;
        $this->newsRepository = $newsRepository;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $news_id = $this->getRequest()->getParam('news_id');
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($news_id) {
            try {
                $this->newsRepository->delete($this->newsRepository->getById($news_id));
                $this->messageManager->addSuccessMessage(__('Your news post has been deleted!'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['news_id' => $news_id]);
            }
        }
        $this->messageManager->addErrorMessage(__('A news post to delete not found.'));
        return $resultRedirect->setPath('*/*/');
    }
}
