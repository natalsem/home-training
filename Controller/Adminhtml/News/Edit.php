<?php

namespace Cyberhull\TheNews\Controller\Adminhtml\News;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Cyberhull\TheNews\Model\NewsFactory;
use Cyberhull\TheNews\Model\NewsRepository;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;


/**
 * Class Edit
 * @package Cyberhull\TheNews\Controller\Adminhtml\News
 */
class Edit extends Action
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var NewsFactory
     */
    protected $newsFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var NewsRepository
     */
    protected $newsRepository;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param NewsFactory $newsFactory
     * @param NewsRepository $newsRepository
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        NewsRepository $newsRepository,
        NewsFactory $newsFactory
    ) {
        $this->coreRegistry = $registry;
        $this->resultPageFactory = $resultPageFactory;
        $this->newsRepository = $newsRepository;
        $this->newsFactory = $newsFactory;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * Edit Department
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $news_id = $this->getRequest()->getParam('news_id');
        $resultRedirect = $this->resultRedirectFactory->create();
      //  $model = $this->newsRepository->getById($news_id);
        $model = $this->newsFactory->create();
        if ($news_id) {
            if (!$this->newsRepository->getById($news_id)->getId()) {
                $this->messageManager->addErrorMessage(__('This news post not exists.'));
                return $resultRedirect->setPath('*/*/');
            }
        }
      //  $model = $this->newsRepository->getById($news_id);
        $data = $this->_getSession()->getFormData(true);
//        if (!empty($data)) {
//            $model->setData($data);
//        }

     //   $this->coreRegistry->register('jobs_department', $model);

        $this->coreRegistry->register('cyberhull_thenews', $model);
        $resultPage = $this->_initAction();
//        $text = __('New News');
//        if ($id) {
//            $text = __('Edit News');
//        }
//

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
     //   $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $news_id ? __('Edit News Post') : __('New post'),
            $news_id ? __('Edit News Post') : __('New post')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('News'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New post'));

        return $resultPage;
    }
}
