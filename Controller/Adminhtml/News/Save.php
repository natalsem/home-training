<?php
namespace Cyberhull\TheNews\Controller\Adminhtml\News;

use Magento\Backend\App\Action;
use Cyberhull\TheNews\Model\NewsFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Cyberhull\News\TheNews\NewsRepository;

/**
 * Class Save News
 *
 * @package Cyberhull\News\Controller\Adminhtml\News
 */
class Save extends Action
{



    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var NewsFactory;
     */
    protected $newsFactory;

    /**
     * @var NewsRepository
     */
    protected $newsRepository;


    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param NewsRepository $newsRepository
     * @param DataPersistorInterface $dataPersistor
     * @param NewsFactory $newsFactory
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Action\Context $context,
        NewsRepository $newsRepository,
        DataPersistorInterface $dataPersistor,
        NewsFactory $newsFactory
    )
    {
        $this->newsRepository = $newsRepository;
        $this->dataPersistor = $dataPersistor;
        $this->newsFactory = $newsFactory;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $data = $this->prepareData($data);
            $id = $this->getRequest()->getParam('news_id');

            if ($id) {
                $model = $this->newsRepository->getById($id);
                $textMessage = "The news was update";
            } else {
                $model = $this->newsFactory->create();
                $textMessage = "The news was create";
            }

            $model->addData($data);

            $this->newsRepository->save($model);
            $this->messageManager->addSuccessMessage(__($textMessage));

            return $resultRedirect->setPath('*/*/');
        } else {
            $this->messageManager->addSuccessMessage(__('Something went wrong try later'));
            return $resultRedirect->setPath('*/*/');
        }
    }

    /**
     * @param $preData
     * @return array
     * @throws LocalizedException
     */
    protected function prepareData($preData)
    {
        $data['store_id'] = $preData['store_id'];
        $data['main_image'] = null;
        $data['code'] = $preData['code'];
        $data['name'] = $preData['name'];
        $data['content'] = $preData['content'];
        $data['main_image'] = 'adsf';
//        if (!empty($preData['main_image'][0]['file'])) {
//            $data['main_image'] = $preData['main_image'][0]['file'];
//        }
        $data['sort'] = $preData['sort'];

        return $data;
    }
}
