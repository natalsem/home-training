<?php
/**
 *    * Short description
 *    *
 *    * @category CyberHULL
 *    * @package m2ee
 *    * @copyright Copyright (C) 2019 CyberHULL (www.cyberhull.com)
 *    * @author  Natalia Sekulich <natalia.sekulich@cyberhull.com>
 */

namespace Cyberhull\TheNews\Controller\Adminhtml\News;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Cyberhull\TheNews\Model\NewsFactory;
use Cyberhull\TheNews\Model\NewsRepository;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Forward;
use Magento\Backend\Model\View\Result\ForwardFactory;

class Add extends Action
{
    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * Add constructor.
     *
     * @param Context $context
     * @param ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    /**
     * @return Forward|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
